obj-m       := muenfs.o
muenfs-objs := main.o fs.o

KERNEL_SOURCE ?= /lib/modules/$(shell uname -r)/build

PWD := $(shell pwd)

all: compile-module compile-test

compile-module:
	$(MAKE) -C $(KERNEL_SOURCE) M=$(PWD) modules

compile-test:
	$(MAKE) -C test all

install: install-module install-test

install-module:
	install -d $(DESTDIR)/lib/modules/extra
	install -m 644 muenfs.ko $(DESTDIR)/lib/modules/extra

install-test:
	$(MAKE) -C test install

clean:
	$(MAKE) -C $(KERNEL_SOURCE) M=$(PWD) clean
	$(MAKE) -C test clean
